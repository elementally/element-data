package design.swirl.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Data tables for physical elements in the periodic table, with accessor methods.
 * <p>
 * This work is:
 * Copyright (c) 2020, Swirl Design (Pty) Ltd <elements@swirl.tech>, and is
 * licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * See: https://creativecommons.org/licenses/by-sa/3.0/legalcode
 * and: https://creativecommons.org/licenses/by-sa/3.0/
 * <p>
 * UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, WE OFFER THE
 * WORK AS-IS AND MAKE NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING
 * THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT
 * LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS,
 * ACCURACY, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
 * SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH
 * EXCLUSION MAY NOT APPLY TO YOU.
 * <p>
 * EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL WE BE
 * LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL,
 * PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE
 * WORK, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * <p>
 * Sources:
 * - http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
 * obtained from the Wayback Machine snapshot for 2016-12-03
 * accessed on 2019-04-19 (main source)
 * - IUPAC Periodic Table of the Elements, 1 December 2018
 * - IUPAC Red Book, 2005
 * - wikipedia, various pages (see below)
 * <p>
 * Most of the data contained in this class derives from the above mentioned
 * website of Penn State University CS Department via the Wayback Machine.
 * No copyright notice or licencing information was found on the site.
 * It is assumed that this dataset is either in the public domain or available
 * under a liberal licence.
 * Changes have been made to some values to make them accurate and current.
 * <p>
 * The compilation of data related to element densities & crystal structures may be:
 * Copyright (c) 2018, Wikipedia editors and contributors, and is
 * licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * The used values are unchanged, but are intended to be kept accurate and current.
 * <p>
 * Note: Only this class is licensed under the terms mentioned above.
 * Swirl Design (Pty) Ltd has other software and libraries which are proprietary,
 * and to which different licenses may apply, for example the code for the app
 * "Glimpse Elements", and in particular, the "Swirl Library" and the code for 
 * the "Glimpse" interaction style.
 * Please contact Swirl Design if you want to use the "Swirl" or "Glimpse"
 * interaction techniques in your own programs.
 */
class ElementData {

    /**
     * The number of elements provided by this class.
     */
    private static final int NUMBER_OF_ELEMENTS = 118;

    /**
     * A (mostly informal) grouping of properties to keep tables manageable.
     */
    private enum PropertyType {
        COLLECTIVE, PHYSICAL, CHEMICAL, QUANTUM, HISTORICAL
    }

    /**
     * The names of the available element properties.
     */
    enum PropertyName {
        ATOMIC_NUMBER,
        PERIOD, GROUP, BLOCK, CATEGORY, //collective
        ATOMIC_WEIGHT, DENSITY, MELTING_POINT, BOILING_POINT,
        IONIZATION_ENERGY, ELECTRON_AFFINITY,
        ATOMIC_RADIUS, VANDERWAALS_RADIUS, IONIC_RADIUS, //physical
        BONDING_TYPE, STANDARD_STATE, CRYSTAL_STRUCTURE,
        OXIDATION_STATES, //chemical
        ELECTRON_CONFIGURATION, ELECTRONEGATIVITY, //quantum
        DISCOVERY_DATE, DISCOVERER, JMOL_COLOR //historical
    }

    /**
     * Get the number of elements for which data are available.
     *
     * @return the number of elements
     */
    static int getNumberOfElements() {
        return NUMBER_OF_ELEMENTS;
    }

    /**
     * Get the number of properties which are defined for at least some elements.
     *
     * @return the number of properties
     */
    static int getNumberOfProperties() {
        return PROPERTY_SEQUENCE.size();
    }

    /**
     * Get the {@link Identity} of an element.
     *
     * @param atomicNumber the atomic number of the element
     * @return the {@link Identity}
     */
    static Identity getIdentity(int atomicNumber) {
        return Identity.get(atomicNumber);
    }

    /**
     * Get the {@link Position} of an element in the periodic table.
     *
     * @param atomicNumber the atomic number of the element
     * @return the {@link Position} in the periodic table
     */
    static Position getPosition(int atomicNumber) {
        return Position.get(atomicNumber);
    }

    /**
     * Get the value of a single property of an element.
     *
     * @param atomicNumber the atomic number of the element
     * @param propertyName the {@link PropertyName} of the property
     * @return the value of the property of the element
     */
    static String getPropertyValue(int atomicNumber, PropertyName propertyName) {
        //extract the value from the last position in the bundle
        String[] bundle = getPropertyBundle(atomicNumber, propertyName);
        return bundle[bundle.length - 1];
    }

    /**
     * Get the (headings, value) bundle for a property of an element.
     *
     * @param atomicNumber  the atomic number of the element
     * @param propertyIndex the index of the property
     * @return the bundle of property headings and value
     */
    static String[] getPropertyBundle(int atomicNumber, int propertyIndex) {
        PropertyName propertyName = getPropertyName(propertyIndex);
        return getPropertyBundle(atomicNumber, propertyName);
    }

    /**
     * Get the (headings, value) bundle for a property of an element.
     *
     * @param atomicNumber the atomic number of the element
     * @param propertyName the {@link PropertyName} of the property
     * @return the bundle of property headings and value
     */
    private static String[] getPropertyBundle(
            int atomicNumber, PropertyName propertyName) {
        String[] bundle;
        PropertySource source = PROPERTY_SOURCE_MAP.get(propertyName);
        if (source == null) {
            throw new IndexOutOfBoundsException(
                    "Source not specified for " + propertyName);
        } else {
            switch (source.propertyType) {
                case COLLECTIVE:
                    bundle = CollectiveProperties
                            .getBundle(atomicNumber, source.propertyIndex);
                    break;
                case PHYSICAL:
                    bundle = PhysicalProperties
                            .getBundle(atomicNumber, source.propertyIndex);
                    break;
                case CHEMICAL:
                    bundle = ChemicalProperties
                            .getBundle(atomicNumber, source.propertyIndex);
                    break;
                case QUANTUM:
                    bundle = QuantumProperties
                            .getBundle(atomicNumber, source.propertyIndex);
                    break;
                case HISTORICAL:
                    bundle = HistoricalProperties
                            .getBundle(atomicNumber, source.propertyIndex);
                    break;
                default:
                    throw new IndexOutOfBoundsException(
                            "Source not impelemented for " + source.propertyType);
            }
        }
        return bundle;
    }

    /**
     * The source where a property's value can be found for every element.
     * The property type and index specify a unique source.
     */
    private static class PropertySource {
        final PropertyType propertyType;
        final int propertyIndex;

        PropertySource(PropertyType propertyType, int propertyIndex) {
            this.propertyType = propertyType;
            this.propertyIndex = propertyIndex;
        }
    }

    /**
     * Get the name of a property.
     *
     * @param index the index of the property
     * @return the property name
     */
    private static PropertyName getPropertyName(int index) {
        PropertyName propertyName = PROPERTY_SEQUENCE.get(index);
        if (propertyName == null) {
            throw new IndexOutOfBoundsException(
                    "Property not specified for sequence no " + index);
        } else {
            return propertyName;
        }
    }

    /**
     * Sequence in which to return the properties.
     * todo: make this configurable
     */
    private static final Map<Integer, PropertyName> PROPERTY_SEQUENCE =
            Collections.unmodifiableMap(new HashMap<Integer, PropertyName>() {{
                put(0, PropertyName.CATEGORY);
                put(1, PropertyName.STANDARD_STATE);
                put(2, PropertyName.ATOMIC_WEIGHT);
                put(3, PropertyName.DENSITY);
                put(4, PropertyName.MELTING_POINT);
                put(5, PropertyName.BOILING_POINT);
                put(6, PropertyName.BONDING_TYPE);
                put(7, PropertyName.PERIOD);
                put(8, PropertyName.GROUP);
                put(9, PropertyName.BLOCK);
                put(10, PropertyName.IONIZATION_ENERGY);
                put(11, PropertyName.ELECTRON_AFFINITY);
                put(12, PropertyName.ATOMIC_RADIUS);
                put(13, PropertyName.VANDERWAALS_RADIUS);
                put(14, PropertyName.IONIC_RADIUS);
                put(15, PropertyName.OXIDATION_STATES);
                put(16, PropertyName.ELECTRONEGATIVITY);
                put(17, PropertyName.JMOL_COLOR);
                put(18, PropertyName.ELECTRON_CONFIGURATION);
                put(19, PropertyName.CRYSTAL_STRUCTURE);
                put(20, PropertyName.DISCOVERER);
                put(21, PropertyName.DISCOVERY_DATE);
            }});

    /**
     * Map of every property name to its index in its source class.
     */
    private static final Map<PropertyName, PropertySource> PROPERTY_SOURCE_MAP =
            Collections.unmodifiableMap(new HashMap<PropertyName, PropertySource>() {{
                put(PropertyName.CATEGORY, new PropertySource(
                        PropertyType.COLLECTIVE, CollectiveProperties.CATEGORY));
                put(PropertyName.PERIOD, new PropertySource(
                        PropertyType.COLLECTIVE, CollectiveProperties.PERIOD));
                put(PropertyName.GROUP, new PropertySource(
                        PropertyType.COLLECTIVE, CollectiveProperties.GROUP));
                put(PropertyName.BLOCK, new PropertySource(
                        PropertyType.COLLECTIVE, CollectiveProperties.BLOCK));
                put(PropertyName.ATOMIC_WEIGHT, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.ATOMIC_WEIGHT));
                put(PropertyName.DENSITY, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.DENSITY));
                put(PropertyName.MELTING_POINT, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.MELTING_POINT));
                put(PropertyName.BOILING_POINT, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.BOILING_POINT));
                put(PropertyName.IONIZATION_ENERGY, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.IONIZATION_ENERGY));
                put(PropertyName.ELECTRON_AFFINITY, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.ELECTRON_AFFINITY));
                put(PropertyName.ATOMIC_RADIUS, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.ATOMIC_RADIUS));
                put(PropertyName.VANDERWAALS_RADIUS, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.VANDERWAALS_RADIUS));
                put(PropertyName.IONIC_RADIUS, new PropertySource(
                        PropertyType.PHYSICAL, PhysicalProperties.IONIC_RADIUS));
                put(PropertyName.BONDING_TYPE, new PropertySource(
                        PropertyType.CHEMICAL, ChemicalProperties.BONDING_TYPE));
                put(PropertyName.STANDARD_STATE, new PropertySource(
                        PropertyType.CHEMICAL, ChemicalProperties.STANDARD_STATE));
                put(PropertyName.CRYSTAL_STRUCTURE, new PropertySource(
                        PropertyType.CHEMICAL, ChemicalProperties.CRYSTAL_STRUCTURE));
                put(PropertyName.OXIDATION_STATES, new PropertySource(
                        PropertyType.CHEMICAL, ChemicalProperties.OXIDATION_STATES));
                put(PropertyName.ELECTRON_CONFIGURATION, new PropertySource(
                        PropertyType.QUANTUM, QuantumProperties.ELECTRON_CONFIGURATION));
                put(PropertyName.ELECTRONEGATIVITY, new PropertySource(
                        PropertyType.QUANTUM, QuantumProperties.ELECTRONEGATIVITY));
                put(PropertyName.DISCOVERY_DATE, new PropertySource(
                        PropertyType.HISTORICAL, HistoricalProperties.DISCOVERY_DATE));
                put(PropertyName.DISCOVERER, new PropertySource(
                        PropertyType.HISTORICAL, HistoricalProperties.DISCOVERER));
                put(PropertyName.JMOL_COLOR, new PropertySource(
                        PropertyType.HISTORICAL, HistoricalProperties.JMOL_COLOR));
            }});

    /**
     * Extract the headings of a property from a map.
     *
     * @param headingsMap   the map of headings
     * @param propertyIndex the property index
     * @return the array of headings (abbreviation, name, unit)
     */
    private static String[] extractHeadings(
            Map<Integer, String[]> headingsMap, int propertyIndex) {
        String[] headings = headingsMap.get(propertyIndex);
        if (headings == null) {
            throw new IndexOutOfBoundsException(
                    "There are no headings for property no " + propertyIndex);
        } else {
            return headings;
        }
    }

    /**
     * Historical properties of an element.
     * Sources:
     * - http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
     * obtained from the Wayback Machine snapshot for 2016-12-03
     * accessed on 2019-04-19.
     */
    private static class HistoricalProperties {
        /**
         * The year in which the element was discovered in CE.
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
         */
        private static final int DISCOVERY_DATE = 1;

        /**
         * The discoverer of the element.
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
         * with name abbreviations and some corrections according to wikipedia.
         */
        private static final int DISCOVERER = 2;

        /**
         * The color for representation of the element in hex RGB.
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
         * They are actually Jmol colors:
         * http://jmol.sourceforge.net/jscolors/
         */
        private static final int JMOL_COLOR = 3;

        /**
         * Get the number of available properties.
         * Some properties may be empty for a particular element.
         *
         * @return the number of available properties
         */
        static int getNumber() {
            return HEADINGS.size();
        }

        /**
         * Get the property bundle for a property of an element.
         *
         * @param atomicNumber  the atomic number of the element
         * @param propertyIndex the index of the property
         * @return the bundle of headings and value of the property
         */
        static String[] getBundle(int atomicNumber, int propertyIndex) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Chemical properties undefined for element no " +
                                atomicNumber);
            }
            String value = HISTORICAL_PROPERTIES[atomicNumber - 1][propertyIndex];
            String[] headings = extractHeadings(HEADINGS, propertyIndex);
            return new String[]{headings[0], headings[1], headings[2], value};
        }

        /**
         * The headings for the historical properties.
         * Three parts: abbreviation/symbol, name, unit.
         */
        private static final Map<Integer, String[]> HEADINGS =
                Collections.unmodifiableMap(new HashMap<Integer, String[]>() {{
                    put(DISCOVERY_DATE, new String[]{"yd", "year discovered", "CE"});
                    put(DISCOVERER, new String[]{"di", "discoverer", ""});
                    put(JMOL_COLOR, new String[]{"jm", "Jmol hex color", "RGB"});
                }});

        /**
         * Historical element properties.
         * Symbol, discovery date, (Jmol) color.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final String[][] HISTORICAL_PROPERTIES = {
                {"H", "1766", "H. Cavendish", "FFFFFF"},
                {"He", "1868", "J. Janssen", "D9FFFF"},
                {"Li", "1817", "J. Arfvedson", "CC80FF"},
                {"Be", "1798", "M.L. Vauquelin", "C2FF00"},
                {"B", "1807", "H. Davy", "FFB5B5"},
                {"C", "ancient", "", "909090"},
                {"N", "1772", "D. Rutherford", "3050F8"},
                {"O", "1774", "J. Priestly", "FF0D0D"},
                {"F", "1670", "G. Gore", "90E050"},
                {"Ne", "1898", "W. Ramsay, M. Travers", "B3E3F5"},
                {"Na", "1807", "H. Davy", "AB5CF2"},
                {"Mg", "1808", "H. Davy", "8AFF00"},
                {"Al", "ancient", "", "BFA6A6"},
                {"Si", "1854", "J.J. Berzelius", "F0C8A0"},
                {"P", "1669", "H. Brand", "FF8000"},
                {"S", "ancient", "", "FFFF30"},
                {"Cl", "1774", "K. Scheele", "1FF01F"},
                {"Ar", "1894", "Rayleigh, W. Ramsay", "80D1E3"},
                {"K", "1807", "H. Davy", "8F40D4"},
                {"Ca", "ancient", "", "3DFF00"},
                {"Sc", "1876", "L.F. Nilson", "E6E6E6"},
                {"Ti", "1791", "W. Gregor", "BFC2C7"},
                {"V", "1803", "A.M. del Rio", "A6A6AB"},
                {"Cr", "ancient", "", "8A99C7"},
                {"Mn", "1774", "J. Gahn", "9C7AC7"},
                {"Fe", "ancient", "", "E06633"},
                {"Co", "ancient", "", "F090A0"},
                {"Ni", "1751", "A.F. Cronstedt", "50D050"},
                {"Cu", "ancient", "", "C88033"},
                {"Zn", "1746", "A.S. Marggraf", "7D80B0"},
                {"Ga", "1875", "P.E.L. de Boisbaudran ", "C28F8F"},
                {"Ge", "1886", "C. Winkler", "668F8F"},
                {"As", "ancient", "", "BD80E3"},
                {"Se", "1817", "J.J. Berzelius", "FFA100"},
                {"Br", "1826", "A. Balard", "A62929"},
                {"Kr", "1898", "W. Ramsay", "5CB8D1"},
                {"Rb", "1861", "R. Bunsen, G. Kirchhoff", "702EB0"}, //*
                {"Sr", "1790", "A. Crawford", "00FF00"},
                {"Y", "1794", "J. Gadolin", "94FFFF"},
                {"Zr", "1789", "M. Klaproth", "94E0E0"},
                {"Nb", "1801", "C. Hatchett", "73C2C9"},
                {"Mo", "1778", "K. Scheele", "54B5B5"},
                {"Tc", "1937", "C. Perrier, E. Segre", "3B9E9E"},
                {"Ru", "1827", "G.W. Osann", "248F8F"},
                {"Rh", "1803", "W. Wollaston", "0A7D8C"},
                {"Pd", "1803", "W. Wollaston", "006985"},
                {"Ag", "ancient", "", "C0C0C0"},
                {"Cd", "1817", "F. Stromeyer ", "FFD98F"},
                {"In", "1863", "F. Reich, T. Richter", "A67573"},
                {"Sn", "ancient", "", "668080"},
                {"Sb", "ancient", "", "9E63B5"},
                {"Te", "1782", "F. Muller", "D47A00"},
                {"I", "1811", "B. Courtois", "940094"},
                {"Xe", "1898", "W. Ramsay, M. Travers", "429EB0"},
                {"Cs", "1860", "R. Bunsen, G. Kirchhoff", "57178F"},
                {"Ba", "1808", "H. Davy", "00C900"},
                {"La", "1839", "C.G. Mosander", "70D4FF"},
                {"Ce", "1803", "Berzelius, W. Hisinger", "FFFFC7"},
                {"Pr", "1885", "C. Welsbach", "D9FFC7"},
                {"Nd", "1885", "C. Welsbach", "C7FFC7"},
                {"Pm", "1947", "Marinsky, Glendenin, Coryell ", "A3FFC7"},
                {"Sm", "1853", "J.C. Galissard", "8FFFC7"},
                {"Eu", "1901", "E.A. Demarcay", "61FFC7"},
                {"Gd", "1880", "J.C. Galissard", "45FFC7"},
                {"Tb", "1843", "G. Mosander", "30FFC7"},
                {"Dy", "1886", "P.E.L. de Boisbaudran ", "1FFFC7"},
                {"Ho", "1878", "T. Cleve", "00FF9C"},
                {"Er", "1842", "G. Mosander", "00E675"},
                {"Tm", "1879", "T. Cleve", "00D452"},
                {"Yb", "1878", "J.C.G de Marignac", "00BF38"},
                {"Lu", "1907", "G. Urbain", "00AB24"},
                {"Hf", "1923", "D. Coster, G. Hevesey", "4DC2FF"},
                {"Ta", "1802", "A. Ekeberg", "4DA6FF"},
                {"W", "1783", "J.J. & F. de Elhuyar", "2194D6"},
                {"Re", "1925", "W. Noddack, I. Tacke, O. Berg", "267DAB"},
                {"Os", "1803", "S. Tennant", "266696"},
                {"Ir", "1803", "S. Tennant", "175487"},
                {"Pt", "ancient", "", "D0D0E0"},
                {"Au", "ancient", "", "FFD123"},
                {"Hg", "ancient", "", "B8B8D0"},
                {"Tl", "1861", "W. Crookes", "A6544D"},
                {"Pb", "ancient", "", "575961"},
                {"Bi", "ancient", "", "9E4FB5"},
                {"Po", "1898", "M. Curie", "AB5C00"},
                {"At", "1940", "D. Corson", "754F45"},
                {"Rn", "1900", "F. Dorn", "428296"},
                {"Fr", "1939", "M. Perey ", "420066"},
                {"Ra", "1898", "Marie & Pierre Curie", "007D00"},
                {"Ac", "1899", "A. Debierne", "70ABFA"},
                {"Th", "1828", "J.J. Berzelius", "00BAFF"},
                {"Pa", "1913", "K. Fajans, O.H. Göhring", "00A1FF"},
                {"U", "1789", "M. Klaproth", "008FFF"},
                {"Np", "1940", "McMillan, P.H. Abelson", "0080FF"},
                {"Pu", "1940", "G.T. Seaborg et. al.", "006BFF"},
                {"Am", "1944", "G.T. Seaborg", "545CF2"},
                {"Cm", "1944", "G.T. Seaborg", "785CE3"},
                {"Bk", "1949", "A. Ghiorso", "8A4FE3"},
                {"Cf", "1950", "A. Ghiorso", "A136D4"},
                {"Es", "1952", "A. Ghiorso", "B31FD4"},
                {"Fm", "1952", "A. Ghiorso", "B31FBA"},
                {"Md", "1955", "G.T. Seaborg", "B30DA6"},
                {"No", "1957", "JINR", "BD0D87"},
                {"Lr", "1961", "LBNL, JINR", "C70066"},
                {"Rf", "1969", "JINR, LBNL", "CC0059"},
                {"Db", "1967", "JINR", "D1004F"},
                {"Sg", "1974", "LBNL", "D90045"},
                {"Bh", "1976", "GFS", "E00038"},
                {"Hs", "1984", "GFS", "E6002E"},
                {"Mt", "1982", "GFS", "EB0026"},
                {"Ds", "1994", "GFS", ""},
                {"Rg", "1994", "GFS", ""},
                {"Cn", "1996", "GFS", ""},
                {"Nh", "2003", "JINR", ""},
                {"Fl", "1998", "JINR", ""},
                {"Mc", "2003", "JINR", ""},
                {"Lv", "2000", "JINR, LLNL", ""},
                {"Ts", "2010", "JINR, LLNL, ORNL", ""},
                {"Og", "2002", "JINR, LLNL", ""},
        };
    }

    /**
     * Quantum properties of an element.
     * Sources:
     * - http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
     * obtained from the Wayback Machine snapshot for 2016-12-03
     * accessed on 2019-04-19.
     */
    private static class QuantumProperties {
        /**
         * The electron configuration of the element.
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
         */
        private static final int ELECTRON_CONFIGURATION = 1;

        /**
         * The electronegativity of the element on the Pauling scale.
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
         */
        private static final int ELECTRONEGATIVITY = 2;

        /**
         * Get the number of available properties.
         * Some properties may be empty for a particular element.
         *
         * @return the number of available properties
         */
        static int getNumber() {
            return HEADINGS.size();
        }

        /**
         * Get the property bundle for a property of an element.
         *
         * @param atomicNumber  the atomic number of the element
         * @param propertyIndex the index of the property
         * @return the bundle of headings and value of the property
         */
        static String[] getBundle(int atomicNumber, int propertyIndex) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Chemical properties undefined for element no " +
                                atomicNumber);
            }
            String value = QUANTUM_PROPERTIES[atomicNumber - 1][propertyIndex];
            String[] headings = extractHeadings(HEADINGS, propertyIndex);
            return new String[]{headings[0], headings[1], headings[2], value};
        }

        /**
         * The headings for the quantum properties.
         * Three parts: abbreviation/symbol, name, unit.
         */
        private static final Map<Integer, String[]> HEADINGS =
                Collections.unmodifiableMap(new HashMap<Integer, String[]>() {{
                    put(ELECTRON_CONFIGURATION, new String[]{"ec", "electron config", ""});
                    put(ELECTRONEGATIVITY, new String[]{"χᵣ", "electronegativity", "pauling scale"});
                }});

        /**
         * Chemical element properties.
         * Symbol, electron configuration, electronegativity.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final String[][] QUANTUM_PROPERTIES = {
                {"H", "1s1", "2.2"},
                {"He", "1s2", ""},
                {"Li", "[He] 2s1", "0.98"},
                {"Be", "[He] 2s2", "1.57"},
                {"B", "[He] 2s2 2p1", "2.04"},
                {"C", "[He] 2s2 2p2", "2.55"},
                {"N", "[He] 2s2 2p3", "3.04"},
                {"O", "[He] 2s2 2p4", "3.44"},
                {"F", "[He] 2s2 2p5", "3.98"},
                {"Ne", "[He] 2s2 2p6", ""},
                {"Na", "[Ne] 3s1", "0.93"},
                {"Mg", "[Ne] 3s2", "1.31"},
                {"Al", "[Ne] 3s2 3p1", "1.61"},
                {"Si", "[Ne] 3s2 3p2", "1.9"},
                {"P", "[Ne] 3s2 3p3", "2.19"},
                {"S", "[Ne] 3s2 3p4", "2.58"},
                {"Cl", "[Ne] 3s2 3p5", "3.16"},
                {"Ar", "[Ne] 3s2 3p6", ""},
                {"K", "[Ar] 4s1", "0.82"},
                {"Ca", "[Ar] 4s2", "1"},
                {"Sc", "[Ar] 3d1 4s2", "1.36"},
                {"Ti", "[Ar] 3d2 4s2", "1.54"},
                {"V", "[Ar] 3d3 4s2", "1.63"},
                {"Cr", "[Ar] 3d5 4s1", "1.66"},
                {"Mn", "[Ar] 3d5 4s2", "1.55"},
                {"Fe", "[Ar] 3d6 4s2", "1.83"},
                {"Co", "[Ar] 3d7 4s2", "1.88"},
                {"Ni", "[Ar] 3d8 4s2", "1.91"},
                {"Cu", "[Ar] 3d10 4s1", "1.9"},
                {"Zn", "[Ar] 3d10 4s2", "1.65"},
                {"Ga", "[Ar] 3d10 4s2 4p1", "1.81"},
                {"Ge", "[Ar] 3d10 4s2 4p2", "2.01"},
                {"As", "[Ar] 3d10 4s2 4p3", "2.18"},
                {"Se", "[Ar] 3d10 4s2 4p4", "2.55"},
                {"Br", "[Ar] 3d10 4s2 4p5", "2.96"},
                {"Kr", "[Ar] 3d10 4s2 4p6", ""},
                {"Rb", "[Kr] 5s1", "0.82"},
                {"Sr", "[Kr] 5s2", "0.95"},
                {"Y", "[Kr] 4d1 5s2", "1.22"},
                {"Zr", "[Kr] 4d2 5s2", "1.33"},
                {"Nb", "[Kr] 4d4 5s1", "1.6"},
                {"Mo", "[Kr] 4d5 5s1", "2.16"},
                {"Tc", "[Kr] 4d5 5s2", "1.9"},
                {"Ru", "[Kr] 4d7 5s1", "2.2"},
                {"Rh", "[Kr] 4d8 5s1", "2.28"},
                {"Pd", "[Kr] 4d10", "2.2"},
                {"Ag", "[Kr] 4d10 5s1", "1.93"},
                {"Cd", "[Kr] 4d10 5s2", "1.69"},
                {"In", "[Kr] 4d10 5s2 5p1", "1.78"},
                {"Sn", "[Kr] 4d10 5s2 5p2", "1.96"},
                {"Sb", "[Kr] 4d10 5s2 5p3", "2.05"},
                {"Te", "[Kr] 4d10 5s2 5p4", "2.1"},
                {"I", "[Kr] 4d10 5s2 5p5", "2.66"},
                {"Xe", "[Kr] 4d10 5s2 5p6", ""},
                {"Cs", "[Xe] 6s1", "0.79"},
                {"Ba", "[Xe] 6s2", "0.89"},
                {"La", "[Xe] 5d1 6s2", "1.1"},
                {"Ce", "[Xe] 4f1 5d1 6s2", "1.12"},
                {"Pr", "[Xe] 4f3 6s2", "1.13"},
                {"Nd", "[Xe] 4f4 6s2", "1.14"},
                {"Pm", "[Xe] 4f5 6s2", "1.13"},
                {"Sm", "[Xe] 4f6 6s2", "1.17"},
                {"Eu", "[Xe] 4f7 6s2", "1.2"},
                {"Gd", "[Xe] 4f7 5d1 6s2", "1.2"},
                {"Tb", "[Xe] 4f9 6s2", "1.2"},
                {"Dy", "[Xe] 4f10 6s2", "1.22"},
                {"Ho", "[Xe] 4f11 6s2", "1.23"},
                {"Er", "[Xe] 4f12 6s2", "1.24"},
                {"Tm", "[Xe] 4f13 6s2", "1.25"},
                {"Yb", "[Xe] 4f14 6s2", "1.1"},
                {"Lu", "[Xe] 4f14 5d1 6s2", "1.27"},
                {"Hf", "[Xe] 4f14 5d2 6s2", "1.3"},
                {"Ta", "[Xe] 4f14 5d3 6s2", "1.5"},
                {"W", "[Xe] 4f14 5d4 6s2", "2.36"},
                {"Re", "[Xe] 4f14 5d5 6s2", "1.9"},
                {"Os", "[Xe] 4f14 5d6 6s2", "2.2"},
                {"Ir", "[Xe] 4f14 5d7 6s2", "2.2"},
                {"Pt", "[Xe] 4f14 5d9 6s1", "2.28"},
                {"Au", "[Xe] 4f14 5d10 6s1", "2.54"},
                {"Hg", "[Xe] 4f14 5d10 6s2", "2"},
                {"Tl", "[Xe] 4f14 5d10 6s2 6p1", "2.04"},
                {"Pb", "[Xe] 4f14 5d10 6s2 6p2", "2.33"},
                {"Bi", "[Xe] 4f14 5d10 6s2 6p3", "2.02"},
                {"Po", "[Xe] 4f14 5d10 6s2 6p4", "2"},
                {"At", "[Xe] 4f14 5d10 6s2 6p5", "2.2"},
                {"Rn", "[Xe] 4f14 5d10 6s2 6p6", ""},
                {"Fr", "[Rn] 7s1", "0.7"},
                {"Ra", "[Rn] 7s2", "0.9"},
                {"Ac", "[Rn] 6d1 7s2", "1.1"},
                {"Th", "[Rn] 6d2 7s2", "1.3"},
                {"Pa", "[Rn] 5f2 6d1 7s2", "1.5"},
                {"U", "[Rn] 5f3 6d1 7s2", "1.38"},
                {"Np", "[Rn] 5f4 6d1 7s2", "1.36"},
                {"Pu", "[Rn] 5f6 7s2", "1.28"},
                {"Am", "[Rn] 5f7 7s2", "1.3"},
                {"Cm", "[Rn] 5f7 6d1 7s2", "1.3"},
                {"Bk", "[Rn] 5f9 7s2", "1.3"},
                {"Cf", "[Rn] 5f10 7s2", "1.3"},
                {"Es", "[Rn] 5f11 7s2", "1.3"},
                {"Fm", "[Rn] 5f12 7s2", "1.3"},
                {"Md", "[Rn] 5f13 7s2", "1.3"},
                {"No", "[Rn] 5f14 7s2", "1.3"},
                {"Lr", "[Rn] 5f14 7s2 7p1", "1.3"},
                {"Rf", "[Rn] 5f14 6d2 7s2", ""},
                {"Db", "[Rn] 5f14 6d3 7s2", ""},
                {"Sg", "[Rn] 5f14 6d4 7s2", ""},
                {"Bh", "[Rn] 5f14 6d5 7s2", ""},
                {"Hs", "[Rn] 5f14 6d6 7s2", ""},
                {"Mt", "[Rn] 5f14 6d7 7s2", ""},
                {"Ds", "[Rn] 5f14 6d9 7s1", ""},
                {"Rg", "[Rn] 5f14 6d10 7s1", ""},
                {"Cn", "[Rn] 5f14 6d10 7s2", ""},
                {"Nh", "[Rn] 5f14 6d10 7s2 7p1", ""},
                {"Fl", "[Rn] 5f14 6d10 7s2 7p2", ""},
                {"Mc", "[Rn] 5f14 6d10 7s2 7p3", ""},
                {"Lv", "[Rn] 5f14 6d10 7s2 7p4", ""},
                {"Ts", "[Rn] 5f14 6d10 7s2 7p5", ""},
                {"Og", "[Rn] 5f14 6d10 7s2 7p6", ""},
        };

    }

    /**
     * Chemical properties of an element.
     * Sources:
     * - http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
     * obtained from the Wayback Machine snapshot for 2016-12-03
     * accessed on 2019-04-19.
     * - https://en.wikipedia.org/wiki/Periodic_table_(crystal_structure)
     * accessed on 2019-04-20.
     */
    private static class ChemicalProperties {
        /**
         * The chemical bonding type of the element
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int BONDING_TYPE = 1;

        /**
         * The standard state of the element (standard temperature, pressure)
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int STANDARD_STATE = 2;

        /**
         * The crystal structure of the element
         * at standard conditions, if solid, or at its melting point
         * from https://en.wikipedia.org/wiki/Periodic_table_(crystal_structure).
         */
        private static final int CRYSTAL_STRUCTURE = 3;

        /**
         * The possible oxidation states of the element
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int OXIDATION_STATES = 4;

        /**
         * Get the number of available properties.
         * Some properties may be empty for a particular element.
         *
         * @return the number of available properties
         */
        static int getNumber() {
            return HEADINGS.size();
        }

        /**
         * Get the property bundle for a property of an element.
         *
         * @param atomicNumber  the atomic number of the element
         * @param propertyIndex the index of the property
         * @return the bundle of headings and value of the property
         */
        static String[] getBundle(int atomicNumber, int propertyIndex) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Chemical properties undefined for element no " +
                                atomicNumber);
            }
            String value = CHEMICAL_PROPERTIES[atomicNumber - 1][propertyIndex];
            String[] headings = extractHeadings(HEADINGS, propertyIndex);
            return new String[]{headings[0], headings[1], headings[2], value};
        }

        /**
         * The headings for the chemical properties.
         * Three parts: abbreviation/symbol, name, unit.
         */
        private static final Map<Integer, String[]> HEADINGS =
                Collections.unmodifiableMap(new HashMap<Integer, String[]>() {{
                    put(BONDING_TYPE, new String[]{"bt", "bonding type", ""});
                    put(STANDARD_STATE, new String[]{"st", "standard state", ""});
                    put(CRYSTAL_STRUCTURE, new String[]{"cs", "crystal structure", ""});
                    put(OXIDATION_STATES, new String[]{"ox", "oxidation states", ""});
                }});

        /**
         * Chemical element properties.
         * Symbol, bonding type, standard state, oxidation states, electron
         * affinity.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final String[][] CHEMICAL_PROPERTIES = {
                {"H", "diatomic", "gas", "HEX", "-1, 1"},
                {"He", "atomic", "gas", "HCP", ""},
                {"Li", "metallic", "solid", "BCC", "1"},
                {"Be", "metallic", "solid", "HCP", "2"},
                {"B", "covalent network", "solid", "RHO", "1, 2, 3"},
                {"C", "covalent network", "solid", "HEX", "-4, -3, -2, -1, 1, 2, 3, 4"},
                {"N", "diatomic", "gas", "HEX", "-3, -2, -1, 1, 2, 3, 4, 5"},
                {"O", "diatomic", "gas", "SC", "-2, -1, 1, 2"},
                {"F", "atomic", "gas", "SC", "-1"},
                {"Ne", "atomic", "gas", "FCC", ""},
                {"Na", "metallic", "solid", "BCC", "-1, 1"},
                {"Mg", "metallic", "solid", "HCP", "1, 2"},
                {"Al", "metallic", "solid", "FCC", "1, 3"},
                {"Si", "metallic", "solid", "DC", "-4, -3, -2, -1, 1, 2, 3, 4"},
                {"P", "covalent network", "solid", "ORTH", "-3, -2, -1, 1, 2, 3, 4, 5"},
                {"S", "covalent network", "solid", "ORTH", "-2, -1, 1, 2, 3, 4, 5, 6"},
                {"Cl", "covalent network", "gas", "ORTH", "-1, 1, 2, 3, 4, 5, 6, 7"},
                {"Ar", "atomic", "gas", "FCC", ""},
                {"K", "metallic", "solid", "BCC", "1"},
                {"Ca", "metallic", "solid", "FCC", "2"},
                {"Sc", "metallic", "solid", "HCP", "1, 2, 3"},
                {"Ti", "metallic", "solid", "HCP", "-1, 2, 3, 4"},
                {"V", "metallic", "solid", "BCC", "-1, 2, 3, 4"},
                {"Cr", "metallic", "solid", "BCC", "-2, -1, 1, 2, 3, 4, 5, 6"},
                {"Mn", "metallic", "solid", "BCC", "-3, -2, -1, 1, 2, 3, 4, 5, 6, 7"},
                {"Fe", "metallic", "solid", "BCC", "-2, -1, 1, 2, 3, 4, 5, 6"},
                {"Co", "metallic", "solid", "HCP", "-1, 1, 2, 3, 4, 5"},
                {"Ni", "metallic", "solid", "FCC", "-1, 1, 2, 3, 4"},
                {"Cu", "metallic", "solid", "FCC", "1, 2, 3, 4"},
                {"Zn", "metallic", "solid", "HCP", "2"},
                {"Ga", "metallic", "solid", "ORTH", "1, 2, 3"},
                {"Ge", "metallic", "solid", "DC", "-4, 1, 2, 3, 4"},
                {"As", "metallic", "solid", "RHO", "-3, 2, 3, 5"},
                {"Se", "metallic", "solid", "HEX", "-2, 2, 4, 6"},
                {"Br", "covalent network", "liquid", "ORTH", "-1, 1, 3, 4, 5, 7"},
                {"Kr", "atomic", "gas", "FCC", "2"},
                {"Rb", "metallic", "solid", "BCC", "1"},
                {"Sr", "metallic", "solid", "FCC", "2"},
                {"Y", "metallic", "solid", "HCP", "1, 2, 3"},
                {"Zr", "metallic", "solid", "HCP", "1, 2, 3, 4"},
                {"Nb", "metallic", "solid", "BCC", "-1, 2, 3, 4, 5"},
                {"Mo", "metallic", "solid", "BCC", "-2, -1, 1, 2, 3, 4, 5, 6"},
                {"Tc", "metallic", "solid", "HCP", "-3, -1, 1, 2, 3, 4, 5, 6, 7"},
                {"Ru", "metallic", "solid", "HCP", "-2, 1, 2, 3, 4, 5, 6, 7, 8"},
                {"Rh", "metallic", "solid", "FCC", "-1, 1, 2, 3, 4, 5, 6"},
                {"Pd", "metallic", "solid", "FCC", "2, 4"},
                {"Ag", "metallic", "solid", "FCC", "1, 2, 3"},
                {"Cd", "metallic", "solid", "HCP", "2"},
                {"In", "metallic", "solid", "TETR", "1, 2, 3"},
                {"Sn", "metallic", "solid", "TETR", "-4, 2, 4"},
                {"Sb", "metallic", "solid", "RHO", "-3, 3, 5"},
                {"Te", "metallic", "solid", "HEX", "-2, 2, 4, 5, 6"},
                {"I", "covalent network", "solid", "ORTH", "-1, 1, 3, 5, 7"},
                {"Xe", "atomic", "gas", "FCC", "2, 4, 6, 8"},
                {"Cs", "metallic", "solid", "BCC", "1"},
                {"Ba", "metallic", "solid", "BCC", "2"},
                {"La", "metallic", "solid", "DHCP", "2, 3"},
                {"Ce", "metallic", "solid", "DHCP/FCC", "2, 3, 4"},
                {"Pr", "metallic", "solid", "DHCP", "2, 3, 4"},
                {"Nd", "metallic", "solid", "DHCP", "2, 3"},
                {"Pm", "metallic", "solid", "DHCP", "3"},
                {"Sm", "metallic", "solid", "RHO", "2, 3"},
                {"Eu", "metallic", "solid", "BCC", "2, 3"},
                {"Gd", "metallic", "solid", "HCP", "1, 2, 3"},
                {"Tb", "metallic", "solid", "HCP", "1, 3, 4"},
                {"Dy", "metallic", "solid", "HCP", "2, 3"},
                {"Ho", "metallic", "solid", "HCP", "3"},
                {"Er", "metallic", "solid", "HCP", "3"},
                {"Tm", "metallic", "solid", "HCP", "2, 3"},
                {"Yb", "metallic", "solid", "FCC", "2, 3"},
                {"Lu", "metallic", "solid", "HCP", "3"},
                {"Hf", "metallic", "solid", "HCP", "2, 3, 4"},
                {"Ta", "metallic", "solid", "BCC/TETR", "-1, 2, 3, 4, 5"},
                {"W", "metallic", "solid", "BCC", "-2, -1, 1, 2, 3, 4, 5, 6"},
                {"Re", "metallic", "solid", "HCP", "-3, -1, 1, 2, 3, 4, 5, 6, 7"},
                {"Os", "metallic", "solid", "HCP", "-2, -1, 1, 2, 3, 4, 5, 6, 7, 8"},
                {"Ir", "metallic", "solid", "FCC", "-3, -1, 1, 2, 3, 4, 5, 6"},
                {"Pt", "metallic", "solid", "FCC", "2, 4, 5, 6"},
                {"Au", "metallic", "solid", "FCC", "-1, 1, 2, 3, 5"},
                {"Hg", "metallic", "liquid", "RHO", "1, 2, 4"},
                {"Tl", "metallic", "solid", "HCP", "1, 3"},
                {"Pb", "metallic", "solid", "FCC", "-4, 2, 4"},
                {"Bi", "metallic", "solid", "RHO", "-3, 3, 5"},
                {"Po", "metallic", "solid", "SC/RHO", "-2, 2, 4, 6"},
                {"At", "covalent network", "solid", "[FCC]", "-1, 1, 3, 5"},
                {"Rn", "atomic", "gas", "FCC", "2"},
                {"Fr", "metallic", "solid", "[BCC]", "1"},
                {"Ra", "metallic", "solid", "BCC", "2"},
                {"Ac", "metallic", "solid", "FCC", "3"},
                {"Th", "metallic", "solid", "FCC", "2, 3, 4"},
                {"Pa", "metallic", "solid", "TETR", "3, 4, 5"},
                {"U", "metallic", "solid", "ORTH", "3, 4, 5, 6"},
                {"Np", "metallic", "solid", "ORTH", "3, 4, 5, 6, 7"},
                {"Pu", "metallic", "solid", "MON", "3, 4, 5, 6, 7"},
                {"Am", "metallic", "solid", "DHCP", "2, 3, 4, 5, 6"},
                {"Cm", "metallic", "solid", "DHCP", "3, 4"},
                {"Bk", "metallic", "solid", "DHCP", "3, 4"},
                {"Cf", "metallic", "solid", "DHCP", "2, 3, 4"},
                {"Es", "", "solid", "FCC", "2, 3"},
                {"Fm", "", "", "[FCC]", "2, 3"},
                {"Md", "", "", "[FCC]", "2, 3"},
                {"No", "", "", "[FCC]", "2, 3"},
                {"Lr", "", "", "[HCP]", "3"},
                {"Rf", "", "", "[HCP]", "4"},
                {"Db", "", "", "[BCC]", ""},
                {"Sg", "", "", "[BCC]", ""},
                {"Bh", "", "", "[HCP]", ""},
                {"Hs", "", "", "[HCP]", ""},
                {"Mt", "", "", "[FCC]", ""},
                {"Ds", "", "", "[BCC]", ""},
                {"Rg", "", "", "[BCC]", ""},
                {"Cn", "", "", "[BCC]", ""},
                {"Nh", "", "", "[HCP]", ""},
                {"Fl", "", "", "[FCC]", ""},
                {"Mc", "", "", "", ""},
                {"Lv", "", "", "", ""},
                {"Ts", "", "", "", ""},
                {"Og", "", "", "", ""},
        };
    }

    /**
     * Physical properties of an element.
     * Sources:
     * - IUPAC Periodic Table of the Elements, 1 December 2018
     * - https://www.nndc.bnl.gov/nudat2
     * - http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm
     * obtained from the Wayback Machine snapshot for 2016-12-03
     * accessed on 2019-04-19.
     * - https://en.wikipedia.org/wiki/Densities_of_the_elements_(data_page)
     * accessed on 2019-04-20.
     * - wikipedia, various other pages
     */
    private static class PhysicalProperties {
        /**
         * The standard atomic weight of the element relative to C₁₂
         * from IUPAC table 1 December 2018 (uncertainty removed); OR
         * the atomic mass number of the longest lived isotope [in brackets]
         * from https://www.nndc.bnl.gov/nudat2 & wikipedia.
         */
        private static final int ATOMIC_WEIGHT = 1;

        /**
         * The density of the element in g/cm³
         * from https://en.wikipedia.org/wiki/Densities_of_the_elements_(data_page).
         */
        private static final int DENSITY = 2;

        /**
         * The melting point of the element in Kelvin
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int MELTING_POINT = 3;

        /**
         * The boiling point of the element in Kelvin
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int BOILING_POINT = 4;

        /**
         * The ionization energy of the element in kJ/mol
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int IONIZATION_ENERGY = 5;

        /**
         * The electron affinity of the element in kJ/mol
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int ELECTRON_AFFINITY = 6;

        /**
         * The atomic radius of the element in picometers
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int ATOMIC_RADIUS = 7;

        /**
         * The van der Waals radius of the element in picometers
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int VANDERWAALS_RADIUS = 8;

        /**
         * The effective (vs crystal) ionic radius of the element in picometers
         * from http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm.
         */
        private static final int IONIC_RADIUS = 9;

        /**
         * Get the number of available properties.
         * Some properties may be empty for a particular element.
         *
         * @return the number of available properties
         */
        static int getNumber() {
            return HEADINGS.size();
        }

        /**
         * Get the property bundle for a property of an element.
         *
         * @param atomicNumber  the atomic number of the element
         * @param propertyIndex the index of the property
         * @return the bundle of headings and value of the property
         */
        static String[] getBundle(int atomicNumber, int propertyIndex) {
            String value = PHYSICAL_PROPERTIES[atomicNumber - 1][propertyIndex];
            String[] headings;
            if (propertyIndex == ATOMIC_WEIGHT && value.startsWith("[")) {
                headings = massNumberHeadings;
            } else {
                headings = extractHeadings(HEADINGS, propertyIndex);
            }
            return new String[]{headings[0], headings[1], headings[2], value};
        }

        /**
         * The headings for the physical properties.
         * Three parts: abbreviation/symbol, name, unit.
         */
        private static final Map<Integer, String[]> HEADINGS =
                Collections.unmodifiableMap(new HashMap<Integer, String[]>() {{
                    put(ATOMIC_WEIGHT, new String[]{"Aᵣ", "atomic weight", "C₁₂/12"});
                    put(DENSITY, new String[]{"ρ", "density", "g/cm³"});
                    put(MELTING_POINT, new String[]{"Tm", "melting point", "Kelvin"});
                    put(BOILING_POINT, new String[]{"Tb", "boiling point", "Kelvin"});
                    put(IONIZATION_ENERGY, new String[]{"Eᵢ", "ionization energy", "kJ/mol"});
                    put(ELECTRON_AFFINITY, new String[]{"ea", "electron affinity", "kJ/mol"});
                    put(ATOMIC_RADIUS, new String[]{"rₐ", "atomic radius", "pm"});
                    //next symbol should be r_w, but subscript_w is missing in Unicode
                    put(VANDERWAALS_RADIUS, new String[]{"rᵥ", "vd Waals radius", "pm"});
                    put(IONIC_RADIUS, new String[]{"rᵢ", "ionic radius", "pm"});
                }});

        /**
         * Mass number headings as an alternative for atomic weight.
         */
        private static final String[] massNumberHeadings =
                new String[]{"A", "mass number", "nucleons"};

        /**
         * Physical element properties.
         * Symbol, atomic weight, density, melting point, boiling point,
         * ionization energy, electron affinity,
         * atomic radius, vanderWaals radius, (effective) ionic radius.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final String[][] PHYSICAL_PROPERTIES = {
                {"H", "1.008", "0.00008988", "14", "20", "1312", "-73", "37", "120", ""},
                {"He", "4.0026", "0.0001786", "", "4", "2372", "0", "32", "140", ""},
                {"Li", "6.94", "0.534", "454", "1615", "520", "-60", "134", "182", "76 (+1)"},
                {"Be", "9.0122", "1.85", "1560", "2743", "900", "0", "90", "", "45 (+2)"},
                {"B", "10.81", "2.34", "2348", "4273", "801", "-27", "82", "", "27 (+3)"},
                {"C", "12.011", "2.267", "3823", "4300", "1087", "-154", "77", "170", "16 (+4)"},
                {"N", "14.007", "0.001251", "63", "77", "1402", "-7", "75", "155", "146 (-3)"},
                {"O", "15.999", "0.001429", "55", "90", "1314", "-141", "73", "152", "140 (-2)"},
                {"F", "18.998", "0.0017", "54", "85", "1681", "-328", "71", "147", "133 (-1)"},
                {"Ne", "20.18", "0.0009002", "25", "27", "2081", "0", "69", "154", ""},
                {"Na", "22.990", "0.968", "371", "1156", "496", "-53", "154", "227", "102 (+1)"},
                {"Mg", "24.305", "1.738", "923", "1363", "738", "0", "130", "173", "72 (+2)"},
                {"Al", "26.982", "2.7", "933", "2792", "578", "-43", "118", "", "53.5 (+3)"},
                {"Si", "28.085", "2.33", "1687", "3173", "787", "-134", "111", "210", "40 (+4)"},
                {"P", "30.974", "1.823", "317", "554", "1012", "-72", "106", "180", "44 (+3)"},
                {"S", "32.06", "1.96", "388", "718", "1000", "-200", "102", "180", "184 (-2)"},
                {"Cl", "35.45", "0.0032", "172", "239", "1251", "-349", "99", "175", "181 (-1)"},
                {"Ar", "39.95", "0.001784", "84", "87", "1521", "0", "97", "188", ""},
                {"K", "39.098", "0.89", "337", "1032", "419", "-48", "196", "275", "138 (+1)"},
                {"Ca", "40.078", "1.55", "1115", "1757", "590", "-2", "174", "", "100 (+2)"},
                {"Sc", "44.956", "2.985", "1814", "3103", "633", "-18", "144", "", "74.5 (+3)"},
                {"Ti", "47.867", "4.506", "1941", "3560", "659", "-8", "136", "", "86 (+2)"},
                {"V", "50.942", "6", "2183", "3680", "651", "-51", "125", "", "79 (+2)"},
                {"Cr", "51.996", "7.15", "2180", "2944", "653", "-64", "127", "", "80 (+2*)"},
                {"Mn", "54.938", "7.21", "1519", "2334", "717", "0", "139", "", "67 (+2)"},
                {"Fe", "55.845", "7.86", "1811", "3134", "763", "-16", "125", "", "78 (+2*)"},
                {"Co", "58.933", "8.9", "1768", "3200", "760", "-64", "126", "", "74.5 (+2*)"},
                {"Ni", "58.693", "8.908", "1728", "3186", "737", "-112", "121", "163", "69 (+2)"},
                {"Cu", "63.546", "8.96", "1358", "3200", "746", "-118", "138", "140", "77 (+1)"},
                {"Zn", "65.38", "7.14", "693", "1180", "906", "0", "131", "139", "74 (+2)"},
                {"Ga", "69.723", "5.91", "303", "2477", "579", "-29", "126", "187", "62 (+3)"},
                {"Ge", "72.63", "5.323", "1211", "3093", "762", "-119", "122", "", "73 (+2)"},
                {"As", "74.922", "5.727", "1090", "887", "947", "-78", "119", "185", "58 (+3)"},
                {"Se", "78.971", "4.81", "494", "958", "941", "-195", "116", "190", "198 (-2)"},
                {"Br", "79.904", "3.1028", "266", "332", "1140", "-325", "114", "185", "196 (-1)"},
                {"Kr", "83.798", "0.003749", "116", "120", "1351", "0", "110", "202", ""},
                {"Rb", "85.468", "1.532", "312", "961", "403", "-47", "211", "", "152 (+1)"},
                {"Sr", "87.62", "2.64", "1050", "1655", "550", "-5", "192", "", "118 (+2)"},
                {"Y", "88.906", "4.472", "1799", "3618", "600", "-30", "162", "", "90 (+3)"},
                {"Zr", "91.224", "6.52", "2128", "4682", "640", "-41", "148", "", "72 (+4)"},
                {"Nb", "92.906", "8.57", "2750", "5017", "652", "-86", "137", "", "72 (+3)"},
                {"Mo", "95.95", "10.28", "2896", "4912", "684", "-72", "145", "", "69 (+3)"},
                {"Tc", "[98]", "11", "2430", "4538", "702", "-53", "156", "", "64.5 (+4)"},
                {"Ru", "101.07", "12.45", "2607", "4423", "710", "-101", "126", "", "68 (+3)"},
                {"Rh", "102.91", "12.41", "2237", "3968", "720", "-110", "135", "", "66.5 (+3)"},
                {"Pd", "106.42", "12.023", "1828", "3236", "804", "-54", "131", "163", "59 (+1)"},
                {"Ag", "107.87", "10.49", "1235", "2435", "731", "-126", "153", "172", "115 (+1)"},
                {"Cd", "112.41", "8.65", "594", "1040", "868", "0", "148", "158", "95 (+2)"},
                {"In", "114.82", "7.31", "430", "2345", "558", "-29", "144", "193", "80 (+3)"},
                {"Sn", "118.71", "7.265", "505", "2875", "709", "-107", "141", "217", "112 (+2)"},
                {"Sb", "121.76", "6.697", "904", "1860", "834", "-103", "138", "", "76 (+3)"},
                {"Te", "127.60", "6.24", "723", "1261", "869", "-190", "135", "206", "221 (-2)"},
                {"I", "126.90", "4.933", "387", "457", "1008", "-295", "133", "198", "220 (-1)"},
                {"Xe", "131.29", "0.005894", "161", "165", "1170", "0", "130", "216", "48 (+8)"},
                {"Cs", "132.91", "1.93", "302", "944", "376", "-46", "225", "", "167 (+1)"},
                {"Ba", "137.33", "3.51", "1000", "2143", "503", "-14", "198", "", "135 (+2)"},
                {"La", "138.91", "6.162", "1193", "3737", "538", "-48", "169", "", "103.2 (+3)"},
                {"Ce", "140.12", "6.77", "1071", "3633", "534", "-50", "", "", "102 (+3)"},
                {"Pr", "140.91", "6.77", "1204", "3563", "527", "-50", "", "", "99 (+3)"},
                {"Nd", "144.24", "7.01", "1294", "3373", "533", "-50", "", "", "129 (+2)"},
                {"Pm", "[145]", "7.26", "1373", "3273", "540", "-50", "", "", "97 (+3)"},
                {"Sm", "150.36", "7.52", "1345", "2076", "545", "-50", "", "", "122 (+2)"},
                {"Eu", "151.96", "5.244", "1095", "1800", "547", "-50", "", "", "117 (+2)"},
                {"Gd", "157.25", "7.9", "1586", "3523", "593", "-50", "", "", "93.8 (+3)"},
                {"Tb", "158.93", "8.23", "1629", "3503", "566", "-50", "", "", "92.3 (+3)"},
                {"Dy", "162.50", "8.54", "1685", "2840", "573", "-50", "", "", "107 (+2)"},
                {"Ho", "164.93", "8.79", "1747", "2973", "581", "-50", "", "", "90.1 (+3)"},
                {"Er", "167.26", "9.066", "1770", "3141", "589", "-50", "", "", "89 (+3)"},
                {"Tm", "168.93", "9.32", "1818", "2223", "597", "-50", "", "", "103 (+2)"},
                {"Yb", "173.05", "6.9", "1092", "1469", "603", "-50", "", "", "102 (+2)"},
                {"Lu", "174.97", "9.841", "1936", "3675", "524", "-50", "160", "", "86.1 (+3)"},
                {"Hf", "178.49", "13.31", "2506", "4876", "659", "0", "150", "", "71 (+4)"},
                {"Ta", "180.95", "16.69", "3290", "5731", "761", "-31", "138", "", "72 (+3)"},
                {"W", "183.84", "19.25", "3695", "5828", "770", "-79", "146", "", "66 (+4)"},
                {"Re", "186.21", "21.02", "3459", "5869", "760", "-15", "159", "", "63 (+4)"},
                {"Os", "190.23", "22.59", "3306", "5285", "840", "-106", "128", "", "63 (+4)"},
                {"Ir", "192.22", "22.56", "2739", "4701", "880", "-151", "137", "", "68 (+3)"},
                {"Pt", "195.08", "21.45", "2041", "4098", "870", "-205", "128", "175", "86 (+2)"},
                {"Au", "196.97", "19.3", "1337", "3129", "890", "-223", "144", "166", "137 (+1)"},
                {"Hg", "200.59", "13.534", "234", "630", "1007", "0", "149", "155", "119 (+1)"},
                {"Tl", "204.38", "11.85", "577", "1746", "589", "-19", "148", "196", "150 (+1)"},
                {"Pb", "207.2", "11.34", "601", "2022", "716", "-35", "147", "202", "119 (+2)"},
                {"Bi", "208.98", "9.78", "544", "1837", "703", "-91", "146", "", "103 (+3)"},
                {"Po", "[209]", "9.196", "527", "1235", "812", "-183", "", "", "94 (+4)"},
                {"At", "[210]", "", "575", "", "920", "-270", "", "", "62 (+7)"},
                {"Rn", "[222]", "0.00973", "202", "211", "1037", "", "145", "", ""},
                {"Fr", "[223]", "1.87 ?", "", "", "380", "", "", "", "180 (+1)"},
                {"Ra", "[226]", "5.5", "973", "2010", "509", "", "", "", "148 (+2)"},
                {"Ac", "[227]", "10", "1323", "3473", "499", "", "", "", "112 (+3)"},
                {"Th", "232.04", "11.7", "2023", "5093", "587", "", "", "", "94 (+4)"},
                {"Pa", "231.04", "15.37", "1845", "4273", "568", "", "", "", "104 (+3)"},
                {"U", "238.03", "19.1", "1408", "4200", "598", "", "", "186", "102.5 (+3)"},
                {"Np", "[237]", "20.2", "917", "4273", "605", "", "", "", "110 (+2)"},
                {"Pu", "[244]", "19.816", "913", "3503", "585", "", "", "", "100 (+3)"},
                {"Am", "[243]", "12", "1449", "2284", "578", "", "", "", "126 (+2)"},
                {"Cm", "[247]", "13.51", "1618", "3383", "581", "", "", "", "97 (+3)"},
                {"Bk", "[247]", "14.78", "1323", "", "601", "", "", "", "96 (+3)"},
                {"Cf", "[251]", "15.1", "1173", "", "608", "", "", "", "95 (+3)"},
                {"Es", "[252]", "8.84", "1133", "", "619", "", "", "", ""},
                {"Fm", "[257]", "", "1800", "", "627", "", "", "", ""},
                {"Md", "[258]", "", "1100", "", "635", "", "", "", ""},
                {"No", "[259]", "", "1100", "", "642", "", "", "", ""},
                {"Lr", "[266]", "", "1900", "", "", "", "", "", ""},
                {"Rf", "[267]", "", "", "", "", "", "", "", ""},
                {"Db", "[268]", "", "", "", "", "", "", "", ""},
                {"Sg", "[269]", "", "", "", "", "", "", "", ""},
                {"Bh", "[270]", "", "", "", "", "", "", "", ""},
                {"Hs", "[270]", "", "", "", "", "", "", "", ""},
                {"Mt", "[278]", "", "", "", "", "", "", "", ""},
                {"Ds", "[281]", "", "", "", "", "", "", "", ""},
                {"Rg", "[281]", "", "", "", "", "", "", "", ""},
                {"Cn", "[285]", "", "", "", "", "", "", "", ""},
                {"Nh", "[286]", "", "", "", "", "", "", "", ""},
                {"Fl", "[289]", "", "", "", "", "", "", "", ""},
                {"Mc", "[289]", "", "", "", "", "", "", "", ""},
                {"Lv", "[293]", "", "", "", "", "", "", "", ""},
                {"Ts", "[294]", "", "", "", "", "", "", "", ""},
                {"Og", "[294]", "", "", "", "", "", "", "", ""},
        };
    }

    /**
     * The collective properties of an element.
     * Sources:
     * - IUPAC Periodic Table of the Elements, 1 December 2018
     * - IUPAC Red Book, 2005
     * - Various other periodic tables (for categories not recognized by IUPAC)
     */
    private static class CollectiveProperties {
        /**
         * The period of the element.
         */
        static final int PERIOD = 1;

        /**
         * The group number of the element, from 1 to 18, or "" if none.
         * Lanthanoids and actinoids are not considered to belong to any group.
         */
        static final int GROUP = 2;

        /**
         * The block of the element, one of s, p, d or f.
         */
        static final int BLOCK = 3;

        /**
         * The category containing the element, some recognized by IUPAC.
         * IUPAC Red Book, 2005, p 51, "collective names"
         * alkali metal: Li, Na, K, Rb, Cs, Fr
         * alkaline earth metal: Be, Mg, Ca, Sr, Ba, Ra
         * pnictogen: N, P, As, Sb, Bi
         * chalcogen: O, S, Se, Te, Po
         * halogen: F, Cl, Br, I, At
         * noble gas: He, Ne, Ar, Kr, Xe, Rn
         * rare earth metal: Sc, Y and the lanthanoids
         * lanthanoid: La, Ce, Pr, Nd, Pm, Sm, Eu, Gd, Tb, Dy, Ho, Er, Tm, Yb, Lu
         * actinoid: Ac, Th, Pa, U, Np, Pu, Am, Cm,Bk, Cf, Es, Fm, Md, No, Lr
         * <p>
         * Categories not recognized by IUPAC are given in brackets:
         * nonmetal, metalloid, transition metal, post-transition metal, unknown
         */
        static final int CATEGORY = 4;

        /**
         * Get the number of available properties.
         * Some properties may be empty for a particular element.
         *
         * @return the number of available properties
         */
        static int getNumber() {
            return COLLECTIVE_HEADINGS.size();
        }

        /**
         * Get the property bundle for a property of an element.
         *
         * @param atomicNumber  the atomic number of the element
         * @param propertyIndex the index of the property
         * @return the bundle of headings and value of the property
         */
        static String[] getBundle(int atomicNumber, int propertyIndex) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Collective properties undefined for element no " +
                                atomicNumber);
            }
            String[] headings = extractHeadings(COLLECTIVE_HEADINGS, propertyIndex);
            String value = COLLECTIVE_PROPERTIES[atomicNumber - 1][propertyIndex];
            return new String[]{headings[0], headings[1], headings[2], value};
        }

        /**
         * The headings for the collective properties.
         * Three parts: abbreviation, name, unit.
         */
        private static final Map<Integer, String[]> COLLECTIVE_HEADINGS =
                Collections.unmodifiableMap(new HashMap<Integer, String[]>() {{
                    put(PERIOD, new String[]{"pr", "period", ""});
                    put(GROUP, new String[]{"gr", "group", ""});
                    put(BLOCK, new String[]{"bl", "block", ""});
                    put(CATEGORY, new String[]{"ca", "category", ""});
                }});

        /**
         * Collective element properties.
         * Symbol, period, group, block, category.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final String[][] COLLECTIVE_PROPERTIES = {
                {"H", "1", "1", "s", "(nonmetal)"},
                {"He", "1", "18", "s", "noble gas"},
                {"Li", "2", "1", "s", "alkali metal"},
                {"Be", "2", "2", "s", "alkaline earth metal"},
                {"B", "2", "13", "p", "(metalloid)"},
                {"C", "2", "14", "p", "(nonmetal)"},
                {"N", "2", "15", "p", "pnictogen, (nonmetal)"},
                {"O", "2", "16", "p", "chalcogen, (nonmetal)"},
                {"F", "2", "17", "p", "halogen, (nonmetal)"},
                {"Ne", "2", "18", "p", "noble gas"},
                {"Na", "3", "1", "s", "alkali metal"},
                {"Mg", "3", "2", "s", "alkaline earth metal"},
                {"Al", "3", "13", "p", "(post transition metal)"},
                {"Si", "3", "14", "p", "(metalloid)"},
                {"P", "3", "15", "p", "pnictogen, (nonmetal)"},
                {"S", "3", "16", "p", "chalcogen, (nonmetal)"},
                {"Cl", "3", "17", "p", "halogen, (nonmetal)"},
                {"Ar", "3", "18", "p", "noble gas"},
                {"K", "4", "1", "s", "alkali metal"},
                {"Ca", "4", "2", "s", "alkaline earth metal"},
                {"Sc", "4", "3", "d", "rare earth (transition) metal"},
                {"Ti", "4", "4", "d", "(transition metal)"},
                {"V", "4", "5", "d", "(transition metal)"},
                {"Cr", "4", "6", "d", "(transition metal)"},
                {"Mn", "4", "7", "d", "(transition metal)"},
                {"Fe", "4", "8", "d", "(transition metal)"},
                {"Co", "4", "9", "d", "(transition metal)"},
                {"Ni", "4", "10", "d", "(transition metal)"},
                {"Cu", "4", "11", "d", "(transition metal)"},
                {"Zn", "4", "12", "d", "(post transition metal)"},
                {"Ga", "4", "13", "p", "(post transition metal)"},
                {"Ge", "4", "14", "p", "(metalloid)"},
                {"As", "4", "15", "p", "pnictogen, (metalloid)"},
                {"Se", "4", "16", "p", "chalcogen, (nonmetal)"},
                {"Br", "4", "17", "p", "halogen, (nonmetal)"},
                {"Kr", "4", "18", "p", "noble gas"},
                {"Rb", "5", "1", "s", "alkali metal"},
                {"Sr", "5", "2", "s", "alkaline earth metal"},
                {"Y", "5", "3", "d", "rare earth (transition) metal"},
                {"Zr", "5", "4", "d", "(transition metal)"},
                {"Nb", "5", "5", "d", "(transition metal)"},
                {"Mo", "5", "6", "d", "(transition metal)"},
                {"Tc", "5", "7", "d", "(transition metal)"},
                {"Ru", "5", "8", "d", "(transition metal)"},
                {"Rh", "5", "9", "d", "(transition metal)"},
                {"Pd", "5", "10", "d", "(transition metal)"},
                {"Ag", "5", "11", "d", "(transition metal)"},
                {"Cd", "5", "12", "d", "(post transition metal)"},
                {"In", "5", "13", "p", "(post transition metal)"},
                {"Sn", "5", "14", "p", "(post transition metal)"},
                {"Sb", "5", "15", "p", "pnictogen, (metalloid)"},
                {"Te", "5", "16", "p", "chalcogen, (metalloid)"},
                {"I", "5", "17", "p", "halogen, (nonmetal)"},
                {"Xe", "5", "18", "p", "noble gas"},
                {"Cs", "6", "1", "s", "alkali metal"},
                {"Ba", "6", "2", "s", "alkaline earth metal"},
                {"La", "6", "3", "d", "lanthanoid, rare earth metal"},
                {"Ce", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Pr", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Nd", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Pm", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Sm", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Eu", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Gd", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Tb", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Dy", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Ho", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Er", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Tm", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Yb", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Lu", "6", "", "f", "lanthanoid, rare earth metal"},
                {"Hf", "6", "4", "d", "(transition metal)"},
                {"Ta", "6", "5", "d", "(transition metal)"},
                {"W", "6", "6", "d", "(transition metal)"},
                {"Re", "6", "7", "d", "(transition metal)"},
                {"Os", "6", "8", "d", "(transition metal)"},
                {"Ir", "6", "9", "d", "(transition metal)"},
                {"Pt", "6", "10", "d", "(transition metal)"},
                {"Au", "6", "11", "d", "(transition metal)"},
                {"Hg", "6", "12", "d", "(post transition metal)"},
                {"Tl", "6", "13", "p", "(post transition metal)"},
                {"Pb", "6", "14", "p", "(post transition metal)"},
                {"Bi", "6", "15", "p", "pnictogen, (post-tr. metal)"},
                {"Po", "6", "16", "p", "chalcogen, (post-tr. metal)"},
                {"At", "6", "17", "p", "halogen, (metalloid)"},
                {"Rn", "6", "18", "p", "noble gas"},
                {"Fr", "7", "1", "s", "alkali metal"},
                {"Ra", "7", "2", "s", "alkaline earth metal"},
                {"Ac", "7", "3", "d", "actinoid"},
                {"Th", "7", "", "f", "actinoid"},
                {"Pa", "7", "", "f", "actinoid"},
                {"U", "7", "", "f", "actinoid"},
                {"Np", "7", "", "f", "actinoid"},
                {"Pu", "7", "", "f", "actinoid"},
                {"Am", "7", "", "f", "actinoid"},
                {"Cm", "7", "", "f", "actinoid"},
                {"Bk", "7", "", "f", "actinoid"},
                {"Cf", "7", "", "f", "actinoid"},
                {"Es", "7", "", "f", "actinoid"},
                {"Fm", "7", "", "f", "actinoid"},
                {"Md", "7", "", "f", "actinoid"},
                {"No", "7", "", "f", "actinoid"},
                {"Lr", "7", "", "f", "actinoid"},
                {"Rf", "7", "4", "d", "(transition metal)"},
                {"Db", "7", "5", "d", "(transition metal)"},
                {"Sg", "7", "6", "d", "(transition metal)"},
                {"Bh", "7", "7", "d", "(transition metal)"},
                {"Hs", "7", "8", "d", "(transition metal)"},
                {"Mt", "7", "9", "d", "(unknown)"},
                {"Ds", "7", "10", "d", "(unknown)"},
                {"Rg", "7", "11", "d", "(unknown)"},
                {"Cn", "7", "12", "d", "(post transition metal)"},
                {"Nh", "7", "13", "p", "(unknown)"},
                {"Fl", "7", "14", "p", "(unknown)"},
                {"Mc", "7", "15", "p", "(unknown)"},
                {"Lv", "7", "16", "p", "(unknown)"},
                {"Ts", "7", "17", "p", "(unknown)"},
                {"Og", "7", "18", "p", "(unknown)"},
        };
    }

    /**
     * The identity of an element in three ways: atomic number, symbol, name.
     * Sources:
     * - IUPAC Periodic Table of the Elements, 1 December 2018
     */
    static class Identity {
        /**
         * The atomic number Z (= number of protons) of the element.
         */
        final String number;

        /**
         * The conventional one- or two-letter symbol of the element.
         */
        final String symbol;

        /**
         * The IUPAC name of the element (lowercase).
         */
        final String name;

        private Identity(String number, String symbol, String name) {
            this.number = number;
            this.symbol = symbol;
            this.name = name;
        }

        /**
         * Get the Identity of an element.
         *
         * @param atomicNumber the atomic number of the element
         * @return the Identity of the element
         */
        static Identity get(int atomicNumber) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Identity undefined for element no " + atomicNumber);
            }
            return IDENTITIES[atomicNumber - 1];
        }

        /**
         * An array of element identities.
         * Atomic number, symbol, IUPAC name.
         * NB: Keep the elements sorted according to increasing atomic number.
         */
        private static final Identity[] IDENTITIES = {
                new Identity("1", "H", "hydrogen"),
                new Identity("2", "He", "helium"),
                new Identity("3", "Li", "lithium"),
                new Identity("4", "Be", "beryllium"),
                new Identity("5", "B", "boron"),
                new Identity("6", "C", "carbon"),
                new Identity("7", "N", "nitrogen"),
                new Identity("8", "O", "oxygen"),
                new Identity("9", "F", "fluorine"),
                new Identity("10", "Ne", "neon"),
                new Identity("11", "Na", "sodium"),
                new Identity("12", "Mg", "magnesium"),
                new Identity("13", "Al", "aluminium"),
                new Identity("14", "Si", "silicon"),
                new Identity("15", "P", "phosphorus"),
                new Identity("16", "S", "sulfur"),
                new Identity("17", "Cl", "chlorine"),
                new Identity("18", "Ar", "argon"),
                new Identity("19", "K", "potassium"),
                new Identity("20", "Ca", "calcium"),
                new Identity("21", "Sc", "scandium"),
                new Identity("22", "Ti", "titanium"),
                new Identity("23", "V", "vanadium"),
                new Identity("24", "Cr", "chromium"),
                new Identity("25", "Mn", "manganese"),
                new Identity("26", "Fe", "iron"),
                new Identity("27", "Co", "cobalt"),
                new Identity("28", "Ni", "nickel"),
                new Identity("29", "Cu", "copper"),
                new Identity("30", "Zn", "zinc"),
                new Identity("31", "Ga", "gallium"),
                new Identity("32", "Ge", "germanium"),
                new Identity("33", "As", "arsenic"),
                new Identity("34", "Se", "selenium"),
                new Identity("35", "Br", "bromine"),
                new Identity("36", "Kr", "krypton"),
                new Identity("37", "Rb", "rubidium"),
                new Identity("38", "Sr", "strontium"),
                new Identity("39", "Y", "yttrium"),
                new Identity("40", "Zr", "zirconium"),
                new Identity("41", "Nb", "niobium"),
                new Identity("42", "Mo", "molybdenum"),
                new Identity("43", "Tc", "technetium"),
                new Identity("44", "Ru", "ruthenium"),
                new Identity("45", "Rh", "rhodium"),
                new Identity("46", "Pd", "palladium"),
                new Identity("47", "Ag", "silver"),
                new Identity("48", "Cd", "cadmium"),
                new Identity("49", "In", "indium"),
                new Identity("50", "Sn", "tin"),
                new Identity("51", "Sb", "antimony"),
                new Identity("52", "Te", "tellurium"),
                new Identity("53", "I", "iodine"),
                new Identity("54", "Xe", "xenon"),
                new Identity("55", "Cs", "caesium"),
                new Identity("56", "Ba", "barium"),
                new Identity("57", "La", "lanthanum"),
                new Identity("58", "Ce", "cerium"),
                new Identity("59", "Pr", "praseodymium"),
                new Identity("60", "Nd", "neodymium"),
                new Identity("61", "Pm", "promethium"),
                new Identity("62", "Sm", "samarium"),
                new Identity("63", "Eu", "europium"),
                new Identity("64", "Gd", "gadolinium"),
                new Identity("65", "Tb", "terbium"),
                new Identity("66", "Dy", "dysprosium"),
                new Identity("67", "Ho", "holmium"),
                new Identity("68", "Er", "erbium"),
                new Identity("69", "Tm", "thulium"),
                new Identity("70", "Yb", "ytterbium"),
                new Identity("71", "Lu", "lutetium"),
                new Identity("72", "Hf", "hafnium"),
                new Identity("73", "Ta", "tantalum"),
                new Identity("74", "W", "tungsten"),
                new Identity("75", "Re", "rhenium"),
                new Identity("76", "Os", "osmium"),
                new Identity("77", "Ir", "iridium"),
                new Identity("78", "Pt", "platinum"),
                new Identity("79", "Au", "gold"),
                new Identity("80", "Hg", "mercury"),
                new Identity("81", "Tl", "thallium"),
                new Identity("82", "Pb", "lead"),
                new Identity("83", "Bi", "bismuth"),
                new Identity("84", "Po", "polonium"),
                new Identity("85", "At", "astatine"),
                new Identity("86", "Rn", "radon"),
                new Identity("87", "Fr", "francium"),
                new Identity("88", "Ra", "radium"),
                new Identity("89", "Ac", "actinium"),
                new Identity("90", "Th", "thorium"),
                new Identity("91", "Pa", "protactinium"),
                new Identity("92", "U", "uranium"),
                new Identity("93", "Np", "neptunium"),
                new Identity("94", "Pu", "plutonium"),
                new Identity("95", "Am", "americium"),
                new Identity("96", "Cm", "curium"),
                new Identity("97", "Bk", "berkelium"),
                new Identity("98", "Cf", "californium"),
                new Identity("99", "Es", "einsteinium"),
                new Identity("100", "Fm", "fermium"),
                new Identity("101", "Md", "mendelevium"),
                new Identity("102", "No", "nobelium"),
                new Identity("103", "Lr", "lawrencium"),
                new Identity("104", "Rf", "rutherfordium"),
                new Identity("105", "Db", "dubnium"),
                new Identity("106", "Sg", "seaborgium"),
                new Identity("107", "Bh", "bohrium"),
                new Identity("108", "Hs", "hassium"),
                new Identity("109", "Mt", "meitnerium"),
                new Identity("110", "Ds", "darmstadtium"),
                new Identity("111", "Rg", "roentgenium"),
                new Identity("112", "Cn", "copernicium"),
                new Identity("113", "Nh", "nihonium"),
                new Identity("114", "Fl", "flerovium"),
                new Identity("115", "Mc", "moscovium"),
                new Identity("116", "Lv", "livermorium"),
                new Identity("117", "Ts", "tennessine"),
                new Identity("118", "Og", "oganesson"),
                new Identity("119", "", ""),
                new Identity("120", "", ""),
        };
    }

    /**
     * The position (row & column) of an element in the periodic table.
     * Column is the same as group, except for the lanthanoids and actinoids,
     * which are generally not considered to belong to any group.
     * Row is the same as period, except for lanthanoids & actinoids.
     * Sources:
     * - Generated according to:
     * IUPAC Periodic Table of the Elements, 1 December 2018
     * except for the lanthanoics & actinoids.
     */
    static class Position {
        /**
         * The row in the table occupied by the element.
         */
        final int row;

        /**
         * The column in the table occupied by the element.
         */
        final int column;

        private Position(int row, int column) {
            this.row = row;
            this.column = column;
        }

        /**
         * Get the Position of an element (in the periodic table).
         *
         * @param atomicNumber the atomic number of the element
         * @return the Position of the element
         */
        static Position get(int atomicNumber) {
            if (atomicNumber < 1 || atomicNumber > NUMBER_OF_ELEMENTS) {
                throw new IndexOutOfBoundsException(
                        "Position undefined for element no " + atomicNumber);
            }
            Position position = TABLE_POSITIONS.get(atomicNumber);
            if (position == null) {
                throw new IndexOutOfBoundsException(
                        "Position undefined for element no " + atomicNumber);
            } else {
                return position;
            }
        }

        /**
         * Map from the atomic number to an element's Position.
         */
        private static final Map<Integer, Position> TABLE_POSITIONS =
                Collections.unmodifiableMap(
                        new HashMap<Integer, Position>() {{
                    put(1, new Position(1, 1));
                    put(2, new Position(1, 18));

                    put(3, new Position(2, 1));
                    put(4, new Position(2, 2));
                    put(5, new Position(2, 13));
                    put(6, new Position(2, 14));
                    put(7, new Position(2, 15));
                    put(8, new Position(2, 16));
                    put(9, new Position(2, 17));
                    put(10, new Position(2, 18));

                    put(11, new Position(3, 1));
                    put(12, new Position(3, 2));
                    put(13, new Position(3, 13));
                    put(14, new Position(3, 14));
                    put(15, new Position(3, 15));
                    put(16, new Position(3, 16));
                    put(17, new Position(3, 17));
                    put(18, new Position(3, 18));

                    put(19, new Position(4, 1));
                    put(20, new Position(4, 2));
                    put(21, new Position(4, 3));
                    put(22, new Position(4, 4));
                    put(23, new Position(4, 5));
                    put(24, new Position(4, 6));
                    put(25, new Position(4, 7));
                    put(26, new Position(4, 8));
                    put(27, new Position(4, 9));
                    put(28, new Position(4, 10));
                    put(29, new Position(4, 11));
                    put(30, new Position(4, 12));
                    put(31, new Position(4, 13));
                    put(32, new Position(4, 14));
                    put(33, new Position(4, 15));
                    put(34, new Position(4, 16));
                    put(35, new Position(4, 17));
                    put(36, new Position(4, 18));

                    put(37, new Position(5, 1));
                    put(38, new Position(5, 2));
                    put(39, new Position(5, 3));
                    put(40, new Position(5, 4));
                    put(41, new Position(5, 5));
                    put(42, new Position(5, 6));
                    put(43, new Position(5, 7));
                    put(44, new Position(5, 8));
                    put(45, new Position(5, 9));
                    put(46, new Position(5, 10));
                    put(47, new Position(5, 11));
                    put(48, new Position(5, 12));
                    put(49, new Position(5, 13));
                    put(50, new Position(5, 14));
                    put(51, new Position(5, 15));
                    put(52, new Position(5, 16));
                    put(53, new Position(5, 17));
                    put(54, new Position(5, 18));

                    put(55, new Position(6, 1));
                    put(56, new Position(6, 2));

                    put(72, new Position(6, 4));
                    put(73, new Position(6, 5));
                    put(74, new Position(6, 6));
                    put(75, new Position(6, 7));
                    put(76, new Position(6, 8));
                    put(77, new Position(6, 9));
                    put(78, new Position(6, 10));
                    put(79, new Position(6, 11));
                    put(80, new Position(6, 12));
                    put(81, new Position(6, 13));
                    put(82, new Position(6, 14));
                    put(83, new Position(6, 15));
                    put(84, new Position(6, 16));
                    put(85, new Position(6, 17));
                    put(86, new Position(6, 18));

                    put(87, new Position(7, 1));
                    put(88, new Position(7, 2));

                    put(104, new Position(7, 4));
                    put(105, new Position(7, 5));
                    put(106, new Position(7, 6));
                    put(107, new Position(7, 7));
                    put(108, new Position(7, 8));
                    put(109, new Position(7, 9));
                    put(110, new Position(7, 10));
                    put(111, new Position(7, 11));
                    put(112, new Position(7, 12));
                    put(113, new Position(7, 13));
                    put(114, new Position(7, 14));
                    put(115, new Position(7, 15));
                    put(116, new Position(7, 16));
                    put(117, new Position(7, 17));
                    put(118, new Position(7, 18));

                    put(57, new Position(9, 3));
                    put(58, new Position(9, 4));
                    put(59, new Position(9, 5));
                    put(60, new Position(9, 6));
                    put(61, new Position(9, 7));
                    put(62, new Position(9, 8));
                    put(63, new Position(9, 9));
                    put(64, new Position(9, 10));
                    put(65, new Position(9, 11));
                    put(66, new Position(9, 12));
                    put(67, new Position(9, 13));
                    put(68, new Position(9, 14));
                    put(69, new Position(9, 15));
                    put(70, new Position(9, 16));
                    put(71, new Position(9, 17));

                    put(89, new Position(10, 3));
                    put(90, new Position(10, 4));
                    put(91, new Position(10, 5));
                    put(92, new Position(10, 6));
                    put(93, new Position(10, 7));
                    put(94, new Position(10, 8));
                    put(95, new Position(10, 9));
                    put(96, new Position(10, 10));
                    put(97, new Position(10, 11));
                    put(98, new Position(10, 12));
                    put(99, new Position(10, 13));
                    put(100, new Position(10, 14));
                    put(101, new Position(10, 15));
                    put(102, new Position(10, 16));
                    put(103, new Position(10, 17));
                }});
    }
}
